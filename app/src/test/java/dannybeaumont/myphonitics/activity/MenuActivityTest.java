package dannybeaumont.myphonitics.activity;


import android.content.Intent;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import dannybeaumont.myphonetics.BuildConfig;
import dannybeaumont.myphonetics.MainActivity;
import dannybeaumont.myphonetics.MenuActivity;
import dannybeaumont.myphonetics.R;
import dannybeaumont.myphonitics.CustomRobolectricGradleTestRunner;

import static org.robolectric.Shadows.shadowOf;


@RunWith(CustomRobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class)
public class MenuActivityTest {
    MenuActivity subject;

    @Before
    public void setup(){
        subject = Robolectric.setupActivity(MenuActivity.class);
    }

    @Test
    public void clickOnSounds() throws Exception {
        subject.findViewById(R.id.sound_button).performClick();
        Intent expected = new Intent(RuntimeEnvironment.application, MainActivity.class);
        Assert.assertEquals(expected,shadowOf(subject).getNextStartedActivity());
    }
}

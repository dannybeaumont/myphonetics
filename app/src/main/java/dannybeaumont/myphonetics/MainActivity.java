package dannybeaumont.myphonetics;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.JsonReader;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.io.StringReader;
import java.util.ArrayList;

import adapter.CardAdapter;
import manager.SpeechManager;
import model.Definition;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = MainActivity.this.getClass().getSimpleName();
    ImageButton speakButton;
    ImageButton lookupButton;
    TextToSpeech speech;
    String word;
    Spinner spinnerFirst;
    Spinner spinnerSecond;
    TextView textView;
    CardAdapter adapter;
    RecyclerView recyclerView;

    private ArrayList<Definition> defined;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        speakButton = (ImageButton) findViewById(R.id.button_speak);
        speakButton.setOnClickListener(this);
        spinnerFirst = (Spinner) findViewById(R.id.spinner);
        spinnerSecond = (Spinner) findViewById(R.id.spinner2);
        defined = new ArrayList<Definition>();
        adapter = new CardAdapter(defined);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(3);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        speech = SpeechManager.getSpeechManager(getApplicationContext()).getTextToSpeech();
        lookupButton = (ImageButton) findViewById(R.id.button_define);
        lookupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWord(getSpinnersString());
                Ion.with(getApplicationContext())
                        .load(getString(R.string.url) + getWord() + getString(R.string.definition))
                        .setHeader(getString(R.string.header_key), getString(R.string.mashape_key))
                        .setHeader("Accept", "application/json")
                        .asJsonObject()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<JsonObject>>() {
                            @Override
                            public void onCompleted(Exception e, Response<JsonObject> result) {
                                if (result != null) {
                                    if (result.getResult() != null) {
                                        JsonArray definitions = result.getResult().getAsJsonArray("definitions");
                                        defined.clear();
                                        for (JsonElement definition : definitions) {
                                            Gson gson = new Gson();
                                            Definition definition1 = gson.fromJson(definition, Definition.class);
                                            defined.add(definition1);
                                        }
                                        if (defined.size() <= 0) {
                                            String notFound = "{\n" +
                                                    "      \"definition\": \"No Definition Found.\",\n" +
                                                    "      \"partOfSpeech\": \"noun\"\n" +
                                                    "    }";
                                            Gson gson = new Gson();
                                            Definition definition = gson.fromJson(notFound,Definition.class);
                                            defined.add(definition);
                                        }
                                        adapter = new CardAdapter(defined);
                                        recyclerView.setAdapter(adapter);
                                    }
                                }
                            }
                        });
            }
        });
    }

    private void speakWords() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            speech.speak(getWord(), TextToSpeech.QUEUE_FLUSH, MainActivity.this.getIntent().getExtras(), null);
        } else {
            speech.speak(getWord(), TextToSpeech.QUEUE_FLUSH, null);
        }
    }
    private void speakWords(String words) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            speech.speak(words, TextToSpeech.QUEUE_FLUSH, MainActivity.this.getIntent().getExtras(), null);
        } else {
            speech.speak(words, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    public void onClick(View v) {
        setWord(getSpinnersString());
        speakWords();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        adapter = new CardAdapter(defined);
        recyclerView.setAdapter(adapter);
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    private String getSpinnersString() {
        return spinnerFirst.getSelectedItem().toString() + spinnerSecond.getSelectedItem().toString();
    }

    public ArrayList<Definition> getDefined() {
        return defined;
    }

    public void speakDefinition(View view){
       String definition = ((TextView)view.findViewById(R.id.definition_text_view)).getText().toString();
        speakWords(definition);
    }

}

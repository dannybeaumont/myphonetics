package adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import dannybeaumont.myphonetics.R;
import holder.CardView;
import model.Definition;

public class CardAdapter extends RecyclerView.Adapter<CardView> {

    List<Definition> cardList;
    public CardAdapter(List<Definition> cardList){
        this.cardList = cardList;
    }

    @Override
    public CardView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_definition_card,parent,false);
        return new CardView(view);
    }

    @Override
    public void onBindViewHolder(CardView holder, int position) {
        Definition definition = cardList.get(position);

        holder.getTextView().setText(definition.getDefinition());
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }
}

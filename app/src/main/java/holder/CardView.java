package holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import dannybeaumont.myphonetics.R;

public class CardView extends RecyclerView.ViewHolder {

    private TextView textView;
    public CardView(View itemView) {
        super(itemView);
        setTextView((TextView) itemView.findViewById(R.id.definition_text_view));
    }
    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }
}

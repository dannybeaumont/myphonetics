package manager;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;

import java.util.Locale;

import dannybeaumont.myphonetics.MainActivity;

public class SpeechManager implements TextToSpeech.OnInitListener {
    private static SpeechManager speechManager;
    private TextToSpeech textToSpeech;
    private Context context;

    private SpeechManager(Context context) {
        this.context = context;
        textToSpeech = new TextToSpeech(context, this);
    }

    public static SpeechManager getSpeechManager(Context context) {
        if (speechManager == null) {
            speechManager = new SpeechManager(context);
        }
        return speechManager;
    }

    public TextToSpeech getTextToSpeech() {
        return textToSpeech;
    }

    public void setTextToSpeech(TextToSpeech textToSpeech) {
        this.textToSpeech = textToSpeech;
    }

    @Override
    public void onInit(int status) {
        if (status != TextToSpeech.ERROR) {
            this.textToSpeech.setLanguage(Locale.US);
        }
    }
}
